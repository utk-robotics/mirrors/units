var searchData=
[
  ['length_5funit_122',['length_unit',['../namespaceunits_1_1category.html#a3dede37fafca5ea729d3c4332ebc3901',1,'units::category']]],
  ['linear_5fscale_123',['linear_scale',['../structunits_1_1linear__scale.html',1,'units::linear_scale&lt; T &gt;'],['../structunits_1_1linear__scale.html#abe50562800503f5e1720ee1af2c6ad0f',1,'units::linear_scale::linear_scale()']]],
  ['linear_5fscale_3c_20double_20_3e_124',['linear_scale&lt; double &gt;',['../structunits_1_1linear__scale.html',1,'units']]],
  ['log_125',['log',['../group___unit_math.html#gaed3e6783dc5ab1dda006913aa53f17c8',1,'units::math']]],
  ['log10_126',['log10',['../group___unit_math.html#gaf02e920d1b57574f1c8ae3974d1e3c80',1,'units::math']]],
  ['log1p_127',['log1p',['../group___unit_math.html#gaef69e3da7b1cf820853dc8bb5043e1e7',1,'units::math']]],
  ['log2_128',['log2',['../group___unit_math.html#gac19a3f10ebced4d7760193130ed220fa',1,'units::math']]],
  ['luminous_5fflux_5funit_129',['luminous_flux_unit',['../namespaceunits_1_1category.html#aaa00c198c610ba934b4c47e0fa98cb87',1,'units::category']]],
  ['luminous_5fintensity_5funit_130',['luminous_intensity_unit',['../namespaceunits_1_1category.html#a4a2e5c0733f4e4ecf8b3d41492658706',1,'units::category']]]
];
