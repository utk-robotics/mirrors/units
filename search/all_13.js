var searchData=
[
  ['value_266',['value',['../classunits_1_1unit__t.html#a5bb6799c6eb6f4cab7b47490ab03b9a1',1,'units::unit_t::value()'],['../structunits_1_1unit__value__add.html#acc92394eedf6c99ca58e084450b2afdd',1,'units::unit_value_add::value()'],['../structunits_1_1unit__value__subtract.html#aa17d1e6270cbb5554fd424698b0d314c',1,'units::unit_value_subtract::value()'],['../structunits_1_1unit__value__multiply.html#a20b685f1942d5a3017700ac7595d7548',1,'units::unit_value_multiply::value()'],['../structunits_1_1unit__value__divide.html#a7580c9bc9ddd8120d11fc76890ba961b',1,'units::unit_value_divide::value()'],['../structunits_1_1unit__value__power.html#a3446bc38fe11fc76264063cd2cbbbe46',1,'units::unit_value_power::value()'],['../structunits_1_1unit__value__sqrt.html#ade4226553e2a1b732b602cf80a86623f',1,'units::unit_value_sqrt::value()']]],
  ['value_5ftype_267',['value_type',['../classunits_1_1unit__t.html#ab292fb5cbcd67da6f7075fac82f0bc36',1,'units::unit_t']]],
  ['velocity_5funit_268',['velocity_unit',['../namespaceunits_1_1category.html#adc650709b31816197530a08cabbd9df2',1,'units::category']]],
  ['voltage_5funit_269',['voltage_unit',['../namespaceunits_1_1category.html#aa6120e2f5e955204cf40247a767f70b0',1,'units::category']]],
  ['volume_5funit_270',['volume_unit',['../namespaceunits_1_1category.html#a546f3cb7c3d969e33487b87a1234d82e',1,'units::category']]]
];
