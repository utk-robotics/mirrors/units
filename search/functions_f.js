var searchData=
[
  ['unit_5fcast_451',['unit_cast',['../group___conversion.html#ga850c3e5c380d1eb1d718128966819c70',1,'units']]],
  ['unit_5ft_452',['unit_t',['../classunits_1_1unit__t.html#ae772ed5c49f98651ff0a731633c4a079',1,'units::unit_t::unit_t()=default'],['../classunits_1_1unit__t.html#acd43a640c3022fdf3e1ed3cea0157e8e',1,'units::unit_t::unit_t(const T value, const Args &amp;... args) noexcept'],['../classunits_1_1unit__t.html#a687c7c408e61d4ec652f3c7b98924c27',1,'units::unit_t::unit_t(const Ty value) noexcept'],['../classunits_1_1unit__t.html#a1f31f268937a30e1b76066813d13431f',1,'units::unit_t::unit_t(const std::chrono::duration&lt; Rep, Period &gt; &amp;value) noexcept'],['../classunits_1_1unit__t.html#a6ae6e3aeeac10dc8d79355e881428d25',1,'units::unit_t::unit_t(const unit_t&lt; UnitsRhs, Ty, NlsRhs &gt; &amp;rhs) noexcept']]]
];
